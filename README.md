# Trabalho 2 - 2022-1 - FSE

Repositório focado no trabalho 2 da matéria de Fundamentos de Sistemas Embarcados feito utilizando a Raspberry Pi.

## Objetivo

Este trabalho tem por objetivo a implementação de um sistema (que simula) o controle de um Air Fryer com controle digital.

## Descrição

Para a solução do projeto, era previsto que trabalhássemos com o envio e solicitação de dados, e isso foi feito utilizando a comunicação serial UART-MODBUS.

- Ao executar o código, o sistema se inicia desligado e parado 
- Com o acionamento do botão 'Ligar' as informações de data, hora e os diferentes tipos de temperatura começam a ser capturados, informados no terminal e gravados em um arquivo CSV. 
- Quando ligado o sistema, ele começa no modo 'manual', onde o usuário seleciona pela Dashboard a temperatura desejada, ficando a cargo do usuário acionar o botão 'Iniciar' para iniciar o processo de cozimento e acionar o 'Parar' para interroper o processo de funcionamento.
- Outra alternativa são os modos 'pré-definidos', escolidos através do botão 'Menu', onde o usuário seleciona um alimento específico, logo, uma temperatura e um tempo específico são enviados através da UART, nesse modo o usuário precisa acionar o botão 'Iniciar', mas não precisa parar manualmente, a não que o mesmo queira.
- Em qualquer um dos modos, 'manual' ou os 'pré-definidos', o forno inicia no modo de aquecimento, onde o tempo só começa a ser contado quando a temperatura atinge o valor de referência.
- Após o tempo terminar em um modo 'pré-definido' ou o usuário acionar o botão 'Parar', o resfriamento do forno e iniciado, e este permanece até a termperatura interna atingir a temperatura ambiente, ou o usuário pressionar 'Ligar'.

- Em todos os passos as informações pertinentes são mostradas no monitor LCD.

**Modos pré-definidos:**
- Batata frita: 35°C por 20 minutos
- Torta: 50°C por 35 minutos
- Frango: 45°C por 25 minutos
- Pao de queijo: 30°C por 15 minutos


## Gráficos retirados do CSV gerado

### Gráfico de temperatura

![Gráfico de temperatura](assets/temperaturas.png)


### Gráfico dos valores de acionamento

![Gráfico dos valores de acionamento](assets/sinal_controle.png)

# **Requisitos**

Para executar o código necessita-se de um ambiente raspbian, por isso é necessário conectar-se a uma Raspberry Pi devidamente configurada.

### **Uso**

1. Clonar este repositório

2. Transferir toda a pasta do repositório para o servidor SSH da RASP.

3. Acessar o servidor SSH da RASP

4. Na pasta raiz do projeto no servidor SSH, bsata executar o comando:

```
make
```

5. Para finalizar a execução do programa, basta apertar CTRL+C no terminal ou pressionar o botão 'Desligar' no Dashboard.

### Observações

- Temperatura ambiente não está sendo lida através so sensor BME280, mas sim definida como uma constante.

### Autor

**Ian Fillipe Pontes Ferreira**
