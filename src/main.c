#include <stdio.h>
#include <signal.h>
#include <stdlib.h>
#include <unistd.h>
#include <wiringPiI2C.h>
#include <wiringPi.h>
#include <softPwm.h>
#include <time.h>

#include "../inc/uart.h"
#include "../inc/pid.h"
#include "../inc/crc16.h"
#include "../inc/control_lcd.h"

// Códigos do Protocolo de Comunicação
unsigned char solicita_temp_interna[7] = {0x01, 0x23, 0xC1, 2, 0, 8, 7};
unsigned char solicita_temp_referencia[7] = {0x01, 0x23, 0xC2, 2, 0, 8, 7};
unsigned char le_comando_usuario[7] = {0x01, 0x23, 0xC3, 2, 0, 8, 7};

unsigned char sinal_controle[7] = {0x01, 0x16, 0xD1, 2, 0, 8, 7};
unsigned char valor_temporizadorInt[7] = {0x01, 0x16, 0xD6, 2, 0, 8, 7};

unsigned char estado_sistema_ligado[8] = {0x01, 0x16, 0xD3, 2, 0, 8, 7, 1};
unsigned char estado_sistema_desligado[8] = {0x01, 0x16, 0xD3, 2, 0, 8, 7, 0};
unsigned char estado_sistema_funcionando[8] = {0x01, 0x16, 0xD5, 2, 0, 8, 7, 1};
unsigned char estado_sistema_parado[8] = {0x01, 0x16, 0xD5, 2, 0, 8, 7, 0};

// ---------------------------------------------

int entrada_usuario = 0, forno_ligado = 0, forno_funcionando = 0, tempo = 1, menu = 0, forno_aquecido = 0, forno_resfriado = 1;
int tempo_definidos[5] = {1, 20, 35, 25, 15};
float temperaturas_definidas[5] = {0, 35.0, 50.0, 45.0, 30.0};
double controle_pid = 0.0;
float temperatura_interna = 0.0, temperatura_referencia = 0.0, temperatura_ambiente = 0.0;
const int GPIO_resistor = 4, GPIO_vetoinha = 5;


void liga_funciona_forno(int modo);
void desliga_para_forno(int modo);
void desliga_sistema(int sig);


int main(){

    uart_init();

    signal(SIGINT, desliga_sistema);

    if (wiringPiSetup() == -1)
        exit(1);

    inicia_lcd();

    if (softPwmCreate(GPIO_resistor, 0, 100) == 0){
        printf("PWM do resistor criado!\n");
    }
    if (softPwmCreate(GPIO_vetoinha, 0, 100) == 0){
        printf("PWM da ventoinha criado!\n");
    }

    pid_configura_constantes(30.0, 0.2, 400.0);

    desliga_para_forno(1);
    desliga_para_forno(2);

    temperatura_referencia = solicita_temperatura(solicita_temp_referencia);
    pid_atualiza_referencia(temperatura_referencia);
    temperatura_ambiente = 26.0;

    FILE *fpt;

    while(1){
        delay(1000);
        struct tm *data_hora_atual;
        time_t segundos; 
        time(&segundos); 
        data_hora_atual = localtime(&segundos); 

        entrada_usuario = solicita_comando_usuario(le_comando_usuario);
        switch(entrada_usuario){
            case 1:
                liga_funciona_forno(1);
                mostra_forno_ligado();
                forno_resfriado = 1;
                break;
            case 2:
                desliga_para_forno(1);
                desliga_sistema(0);
                break;
            case 3:
                liga_funciona_forno(2);
                mostra_forno_parado();
                forno_resfriado = 1;
                break;
            case 4:
                desliga_para_forno(2);
                forno_aquecido = 0;
                forno_resfriado = 0;
                tempo = 1;
                break;
            case 5:
                printf("Mais tempo");
                break;
            case 6:
                printf("Menos tempo");
                break;
            case 7:
                menu++;
                if (menu > 4)
                    menu = 0;
                mostra_menu(menu);
                printf("Menu: %d\n", menu);
                tempo = tempo_definidos[menu];
                break;
            default:
                break;
        }

        if (forno_resfriado == 0){
            fpt = fopen("data/log.csv", "a+");
            pid_atualiza_referencia(temperatura_ambiente);
            temperatura_interna = solicita_temperatura(solicita_temp_interna);
            temperatura_referencia = solicita_temperatura(solicita_temp_referencia);

            if (temperatura_interna - temperatura_ambiente >= -0.1 && temperatura_interna - temperatura_ambiente <= 0.1 && forno_resfriado == 0){
                forno_resfriado = 1;
                printf("Forno resfriado!\n");
                mostra_forno_parado();
            }

            if (forno_resfriado == 0){
                mostra_infos_resfriamento(temperatura_interna, temperatura_ambiente);
            }

            controle_pid = pid_controle(temperatura_interna);

            if(controle_pid > -40 && controle_pid < 0)
                controle_pid = -40;

            envia_sinal_controle(sinal_controle, controle_pid);

            if(controle_pid < 0){
                softPwmWrite(GPIO_resistor, 0);
                delay(0.7);
                softPwmWrite(GPIO_vetoinha, controle_pid*(-1));
                delay(0.7);
            }
            else if(controle_pid > 0){
                softPwmWrite(GPIO_vetoinha, 0);
                delay(0.7);
                softPwmWrite(GPIO_resistor, controle_pid);
                delay(0.7);
            }

            fprintf(fpt, "%d/%d/%d,%d:%d:%d,%.2f,%.2f,%.2f,%.2lf\n", data_hora_atual->tm_mday, data_hora_atual->tm_mon + 1, data_hora_atual->tm_year + 1900, data_hora_atual->tm_hour, data_hora_atual->tm_min, data_hora_atual->tm_sec, temperatura_interna, temperatura_ambiente, temperatura_referencia, controle_pid);
            printf("INFOS: %d/%d/%d,%d:%d:%d,%.2f,%.2f,%.2f,%.2lf\n\n", data_hora_atual->tm_mday, data_hora_atual->tm_mon + 1, data_hora_atual->tm_year + 1900, data_hora_atual->tm_hour, data_hora_atual->tm_min, data_hora_atual->tm_sec, temperatura_interna, temperatura_ambiente, temperatura_referencia, controle_pid);
        }


        if(forno_ligado == 1){
            fpt = fopen("data/log.csv", "a+");
            if (forno_funcionando == 1 && forno_resfriado == 1){
                envia_sinal_controle(valor_temporizadorInt, tempo);

                if (menu == 0){
                    temperatura_referencia = solicita_temperatura(solicita_temp_referencia);
                    pid_atualiza_referencia(temperatura_referencia);
                    if (forno_aquecido == 1){
                        mostra_infos(temperatura_interna, temperatura_referencia, tempo, 'M');
                        tempo++;
                    }
                }
                else if (menu == 1){
                    temperatura_referencia = temperaturas_definidas[menu];
                    pid_atualiza_referencia(temperatura_referencia);
                    if (forno_aquecido == 1){
                        mostra_infos(temperatura_interna, temperatura_referencia, tempo, 'B');
                        tempo--;
                    }
                }
                else if (menu == 2){
                    temperatura_referencia = temperaturas_definidas[menu];
                    pid_atualiza_referencia(temperatura_referencia);
                    if (forno_aquecido == 1){
                        mostra_infos(temperatura_interna, temperatura_referencia, tempo, 'T');
                        tempo--;
                    }
                }
                else if (menu == 3){
                    temperatura_referencia = temperaturas_definidas[menu];
                    pid_atualiza_referencia(temperatura_referencia);
                    if (forno_aquecido == 1){
                        mostra_infos(temperatura_interna, temperatura_referencia, tempo, 'F');
                        tempo--;
                    }
                }
                else if (menu == 4){
                    temperatura_referencia = temperaturas_definidas[menu];
                    pid_atualiza_referencia(temperatura_referencia);
                    if (forno_aquecido == 1){
                        mostra_infos(temperatura_interna, temperatura_referencia, tempo, 'P');
                        tempo--;
                    }
                }

                temperatura_interna = solicita_temperatura(solicita_temp_interna);

                if (temperatura_referencia - temperatura_interna >= -0.1 && temperatura_referencia - temperatura_interna <= 0.1 && forno_aquecido == 0){
                    forno_aquecido = 1;
                    printf("Forno Aquecido!\n");
                }

                if (forno_aquecido == 0){
                    mostra_infos_aquecimento(temperatura_interna, temperatura_referencia);
                }

                controle_pid = pid_controle(temperatura_interna);

                if(controle_pid > -40 && controle_pid < 0)
                    controle_pid = -40;

                envia_sinal_controle(sinal_controle, controle_pid);

                if(controle_pid < 0){
                    softPwmWrite(GPIO_resistor, 0);
                    delay(0.7);
                    softPwmWrite(GPIO_vetoinha, controle_pid*(-1));
                    delay(0.7);
                }
                else if(controle_pid > 0){
                    softPwmWrite(GPIO_vetoinha, 0);
                    delay(0.7);
                    softPwmWrite(GPIO_resistor, controle_pid);
                    delay(0.7);
                }
            }  

            fprintf(fpt, "%d/%d/%d,%d:%d:%d,%.2f,%.2f,%.2f,%.2lf\n", data_hora_atual->tm_mday, data_hora_atual->tm_mon + 1, data_hora_atual->tm_year + 1900, data_hora_atual->tm_hour, data_hora_atual->tm_min, data_hora_atual->tm_sec, temperatura_interna, temperatura_ambiente, temperatura_referencia, controle_pid);
            printf("INFOS: %d/%d/%d,%d:%d:%d,%.2f,%.2f,%.2f,%.2lf\n\n", data_hora_atual->tm_mday, data_hora_atual->tm_mon + 1, data_hora_atual->tm_year + 1900, data_hora_atual->tm_hour, data_hora_atual->tm_min, data_hora_atual->tm_sec, temperatura_interna, temperatura_ambiente, temperatura_referencia, controle_pid);

            if (tempo >= 60)
                tempo = 0;

            if (tempo <= 0){
                desliga_para_forno(2);
                mostra_forno_parado();
                forno_resfriado = 0;
                forno_aquecido = 0;
                tempo = 1;
            }          
        }
        else if(forno_resfriado == 1 && forno_funcionando == 0 && forno_ligado == 1){
            fpt = fopen("data/log.csv", "a+");
            temperatura_interna = solicita_temperatura(solicita_temp_interna);
            temperatura_referencia = solicita_temperatura(solicita_temp_referencia);
            controle_pid = pid_controle(temperatura_interna);
            if(controle_pid > -40 && controle_pid < 0)
                controle_pid = -40;

            fprintf(fpt, "%d/%d/%d,%d:%d:%d,%.2f,%.2f,%.2f,%.2lf\n", data_hora_atual->tm_mday, data_hora_atual->tm_mon + 1, data_hora_atual->tm_year + 1900, data_hora_atual->tm_hour, data_hora_atual->tm_min, data_hora_atual->tm_sec, temperatura_interna, temperatura_ambiente, temperatura_referencia, controle_pid);
            printf("INFOS: %d/%d/%d,%d:%d:%d,%.2f,%.2f,%.2f,%.2lf\n\n", data_hora_atual->tm_mday, data_hora_atual->tm_mon + 1, data_hora_atual->tm_year + 1900, data_hora_atual->tm_hour, data_hora_atual->tm_min, data_hora_atual->tm_sec, temperatura_interna, temperatura_ambiente, temperatura_referencia, controle_pid);
        }

    }   

    uart_close();
    return 0;
}


void liga_funciona_forno(int modo){
    if(modo == 1){
        if(solicita_estado_sistema(estado_sistema_ligado) == 1){
            printf("Forno ligado.\n");
            forno_ligado = 1;
        }
        else{
            printf("Erro ao ligar forno.\n");
        }
    }
    else if(modo == 2){
        if(solicita_estado_sistema(estado_sistema_funcionando)){
            printf("Forno funcionando.\n");
            forno_funcionando = 1;
        }
        else{
            printf("Falha no funcionamento do forno.\n");
        }
    }
}

void desliga_para_forno(int modo){
    if(modo == 1){
        if(solicita_estado_sistema(estado_sistema_desligado) == 1){
            printf("Forno desligado.\n");
            forno_ligado = 0;
        }
        else{
            printf("Erro ao desligar forno.\n");
        }
    }
    else if(modo == 2){
        if(solicita_estado_sistema(estado_sistema_parado)){
            printf("Forno parado.\n");
            forno_funcionando = 0;
        }
        else{
            printf("Falha ao parar forno.\n");
        }
    }
}

void desliga_sistema(int sig){
    desliga_para_forno(1);
    desliga_para_forno(2);

    softPwmWrite(GPIO_resistor, 0);
    delay(0.7);
    softPwmWrite(GPIO_vetoinha, 0);
    delay(0.7);

    ClrLcd();
    printf("Sistema desligado!\n");
    exit(0);
}