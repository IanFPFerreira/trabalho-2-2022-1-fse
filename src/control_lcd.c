#include "../inc/control_lcd.h"
#include <wiringPiI2C.h>
#include <wiringPi.h>
#include <stdlib.h>
#include <stdio.h>

int fd;  // seen by all subroutines

// float to string
void typeFloat(float myFloat)   {
  char buffer[20];
  sprintf(buffer, "%4.2f",  myFloat);
  typeln(buffer);
}

// int to string
void typeInt(int i)   {
  char array1[20];
  sprintf(array1, "%d",  i);
  typeln(array1);
}

// clr lcd go home loc 0x80
void ClrLcd(void)   {
  lcd_byte(0x01, LCD_CMD);
  lcd_byte(0x02, LCD_CMD);
}

// go to location on LCD
void lcdLoc(int line)   {
  lcd_byte(line, LCD_CMD);
}

// out char to LCD at current position
void typeChar(char val)   {

  lcd_byte(val, LCD_CHR);
}

// this allows use of any size string
void typeln(const char *s)   {

  while ( *s ) lcd_byte(*(s++), LCD_CHR);

}

void lcd_byte(int bits, int mode)   {

  //Send byte to data pins
  // bits = the data
  // mode = 1 for data, 0 for command
  int bits_high;
  int bits_low;
  // uses the two half byte writes to LCD
  bits_high = mode | (bits & 0xF0) | LCD_BACKLIGHT ;
  bits_low = mode | ((bits << 4) & 0xF0) | LCD_BACKLIGHT ;

  // High bits
  wiringPiI2CReadReg8(fd, bits_high);
  lcd_toggle_enable(bits_high);

  // Low bits
  wiringPiI2CReadReg8(fd, bits_low);
  lcd_toggle_enable(bits_low);
}

void lcd_toggle_enable(int bits)   {
  // Toggle enable pin on LCD display
  delayMicroseconds(500);
  wiringPiI2CReadReg8(fd, (bits | ENABLE));
  delayMicroseconds(500);
  wiringPiI2CReadReg8(fd, (bits & ~ENABLE));
  delayMicroseconds(500);
}

void lcd_init()   {
  // Initialise display
  lcd_byte(0x33, LCD_CMD); // Initialise
  lcd_byte(0x32, LCD_CMD); // Initialise
  lcd_byte(0x06, LCD_CMD); // Cursor move direction
  lcd_byte(0x0C, LCD_CMD); // 0x0F On, Blink Off
  lcd_byte(0x28, LCD_CMD); // Data length, number of lines, font size
  lcd_byte(0x01, LCD_CMD); // Clear display
  delayMicroseconds(500);
}

void inicia_lcd(){
  fd = wiringPiI2CSetup(I2C_ADDR);
  lcd_init();
  ClrLcd();
}

void mostra_forno_ligado(){
  ClrLcd();
  lcdLoc(LINE1);
  typeln("Forno Ligado");
  lcdLoc(LINE2);
  typeln("Modo: manual");
}

void mostra_forno_funcioando(){
  ClrLcd();
  lcdLoc(LINE1);
  typeln("Forno Iniciando");
}

void mostra_forno_parado(){
  ClrLcd();
  lcdLoc(LINE1);
  typeln("Forno Parado");
}

void mostra_infos(float temperatura_interna, float temperatura_referencia, int tempo, char modo){
  ClrLcd();
  lcdLoc(LINE1);
  typeln("TI:");
  typeFloat(temperatura_interna);
  typeln(" TR:");
  typeFloat(temperatura_referencia);
  lcdLoc(LINE2);
  typeln("Tempo:");
  typeInt(tempo);
  typeln("m MD:");
  typeChar(modo);
}

void mostra_infos_aquecimento(float temperatura_interna, float temperatura_referencia){
  ClrLcd();
  lcdLoc(LINE1);
  typeln("TI:");
  typeFloat(temperatura_interna);
  typeln(" TR:");
  typeFloat(temperatura_referencia);
  lcdLoc(LINE2);
  typeln("Aquecendo...");
}

void mostra_infos_resfriamento(float temperatura_interna, float temperatura_ambiente){
  ClrLcd();
  lcdLoc(LINE1);
  typeln("TI:");
  typeFloat(temperatura_interna);
  typeln(" TA:");
  typeFloat(temperatura_ambiente);
  lcdLoc(LINE2);
  typeln("Resfriando...");
}

void mostra_menu(int modo){
  ClrLcd();
  lcdLoc(LINE1);
  if (modo == 0){
    typeln("Modo:");
    lcdLoc(LINE2);
    typeln("Manual");
  }
  if (modo == 1){
    typeln("Modo:");
    lcdLoc(LINE2);
    typeln("Batata frita");
  }
  if (modo == 2){
    typeln("Modo:");  
    lcdLoc(LINE2);
    typeln("Torta");
  }
  if (modo == 3){
    typeln("Modo:"); 
    lcdLoc(LINE2);
    typeln("Frango");
  } 
  if (modo == 4){
    typeln("Modo:"); 
    lcdLoc(LINE2);
    typeln("Pao de queijo");
  } 
}
